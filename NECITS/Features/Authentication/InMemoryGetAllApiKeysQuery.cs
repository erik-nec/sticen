﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NECITS.Features.Authorization;

namespace NECITS.Features.Authentication
{
    public class InMemoryGetAllApiKeysQuery : IGetAllApiKeysQuery
    {
        public Task<IReadOnlyDictionary<string, ApiKey>> ExecuteAsync()
        {
            var apiKeys = new List<ApiKey>
            {
                new ApiKey(1, "NEC", "60461b75-7351-473a-9a2b-b20d53d6cfd6", new DateTime(2019, 01, 01),
                    new List<string>
                    {
                        Roles.Employee,
                        Roles.Manager
                    }),
                new ApiKey(2, "SWAT", "2510b961-9a44-44d7-8523-5105bd957fbf", new DateTime(2019, 01, 01),
                    new List<string>
                    {
                        Roles.Employee,
                        Roles.Manager
                    }),
                new ApiKey(3, "Third Party", "0cedef17-1942-4e0a-8a9e-32a0c4d6eacf", new DateTime(2019, 06, 01),
                    new List<string>
                    {
                        Roles.ThirdParty
                    })
            };

            IReadOnlyDictionary<string, ApiKey> readonlyDictionary = apiKeys.ToDictionary(x => x.Key, x => x);
            return Task.FromResult(readonlyDictionary);
        }
    }
}
