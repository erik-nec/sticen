﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NECITS.Features.Authentication
{
    public interface IGetAllApiKeysQuery
    {
        Task<IReadOnlyDictionary<string, ApiKey>> ExecuteAsync();
    }
}
