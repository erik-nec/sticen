﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NECITS.Models;
using Microsoft.EntityFrameworkCore;

namespace NECITS.Controllers
{
    #region BusController
    [Route("api/[controller]")]
    [ApiController]
    public class BusController : ControllerBase
    {
        private readonly BusContext _context;

        public BusController(BusContext context)
        {
            _context = context;

            if (_context.Buses.Count() == 0)
            {
                // Create a new Bus if collection is empty,
                // which means you can't delete all Buses.
                _context.Buses.Add(new Bus { ServiceNo = "Item1" });
                _context.Buses.Add(new Bus { ServiceNo = "Item2" });
                _context.SaveChanges();
            }
        }
        #endregion

        ////// GET: api/Buses
        ////[HttpGet]
        ////public async Task<ActionResult<IEnumerable<Bus>>> GetBuses()
        ////{
        ////    return await _context.Buses.ToListAsync();
        ////}

        ////#region snippet_GetByID
        ////// GET: api/Buses/5
        ////[HttpGet("{id}")]
        ////public async Task<ActionResult<Bus>> GetBus(long id)
        ////{
        ////    var bus = await _context.Buses.FindAsync(id);

        ////    if (bus == null)
        ////    {
        ////        return NotFound();
        ////    }

        ////    return bus;
        ////}
        ////#endregion

        ////#region snippet_Update
        ////// PUT: api/Buses/5
        ////[HttpPut("{id}")]
        ////public async Task<IActionResult> PutBus(long id, Bus bus)
        ////{
        ////    if (id != bus.VehicleId)
        ////    {
        ////        return BadRequest();
        ////    }

        ////    _context.Entry(bus).State = EntityState.Modified;

        ////    try
        ////    {
        ////        await _context.SaveChangesAsync();
        ////    }
        ////    catch (DbUpdateConcurrencyException)
        ////    {
        ////        if (!BusExists(id))
        ////        {
        ////            return NotFound();
        ////        }
        ////        else
        ////        {
        ////            throw;
        ////        }
        ////    }

        ////    return NoContent();
        ////}
        ////#endregion

        ////#region snippet_Create
        ////// POST: api/Buses
        ////[HttpPost]
        ////public async Task<ActionResult<Bus>> PostBus(Bus bus)
        ////{
        ////    _context.Buses.Add(bus);
        ////    await _context.SaveChangesAsync();

        ////    //return CreatedAtAction("GetBus", new { id = bus.VehicleId }, bus);
        ////    return CreatedAtAction(nameof(GetBus), new { id = bus.VehicleId }, bus);
        ////}
        ////#endregion

        ////#region snippet_Delete
        ////// DELETE: api/Buses/5
        ////[HttpDelete("{id}")]
        ////public async Task<ActionResult<Bus>> DeleteBus(long id)
        ////{
        ////    var bus = await _context.Buses.FindAsync(id);
        ////    if (bus == null)
        ////    {
        ////        return NotFound();
        ////    }

        ////    _context.Buses.Remove(bus);
        ////    await _context.SaveChangesAsync();

        ////    return bus;
        ////}
        ////#endregion

        ////private bool BusExists(long id)
        ////{
        ////    return _context.Buses.Any(e => e.VehicleId == id);
        ////}
    }
}