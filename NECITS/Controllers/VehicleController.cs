﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NECITS.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace NECITS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly VehicleContext _context;

        public VehicleController(VehicleContext context)
        {
            _context = context;

            //if (_context.Vehicles.Count() == 0)
            //{
            //    // Create a new Vehicle if collection is empty,
            //    // which means you can't delete all Vehicles.
            //    ////_context.Vehicles.Add(new Vehicle { Name = "Item1" });
            //    _context.SaveChanges();
            //}
        }

        #region snippet_GetByID
        ////// GET: api/Vehicles/5
        ////[HttpGet("{vehicleId}")]
        ////public async Task<ActionResult<Vehicle>> GetVehicle(Guid vehicleId)
        ////{
        ////    var vehicle = await _context.Vehicles.FindAsync(vehicleId);

        ////    if (vehicle == null)
        ////    {
        ////        return NotFound();
        ////    }

        ////    return vehicle;
        ////}
        #endregion

        #region snippet_Create
        // POST: api/Vehicles
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Vehicle>> PostVehicle(Vehicle vehicle)
        {
            _context.Vehicles.Add(vehicle);
            vehicle._SeatCount = JsonConvert.SerializeObject(vehicle.SeatCount);
            vehicle._StopList = JsonConvert.SerializeObject(vehicle.StopList);
            await _context.SaveChangesAsync();

            //return CreatedAtAction(nameof(GetVehicle), new { vehicleId = vehicle.VehicleId }, vehicle);
            return new OkObjectResult(new Item { Code = 200, Status = "Success" });
        }
        #endregion

        private bool VehicleExists(Guid vehicleId)
        {
            return _context.Vehicles.Any(e => e.VehicleId == vehicleId);
        }

        public class Item
        {
            public int Code { get; set; }
            public string Status { get; set; }
        }
    }
}