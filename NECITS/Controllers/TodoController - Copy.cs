﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NECITS.Models;
using Microsoft.EntityFrameworkCore;

namespace NECITS.Controllers
{
    #region TodoController
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly TodoContext _context;

        public TodoController(TodoContext context)
        {
            _context = context;

            if (_context.TodoItems.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.TodoItems.Add(new TodoItem { Name = "Item1" });
                _context.SaveChanges();
            }
        }
        #endregion

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetTodoItems()
        {
            return await _context.TodoItems.ToListAsync();
        }

        #region snippet_GetByID
        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
        {
            var todoItem = await _context.TodoItems.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }
        #endregion

        #region snippet_Update
        // PUT: api/TodoItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(long id, TodoItem todoItem)
        {
            if (id != todoItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(todoItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        #endregion

        #region snippet_Create
        // POST: api/TodoItems
        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem todoItem)
        {
            _context.TodoItems.Add(todoItem);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(nameof(GetTodoItem), new { id = todoItem.Id }, todoItem);
        }
        #endregion

        #region snippet_Delete
        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TodoItem>> DeleteTodoItem(long id)
        {
            var todoItem = await _context.TodoItems.FindAsync(id);
            if (todoItem == null)
            {
                return NotFound();
            }

            _context.TodoItems.Remove(todoItem);
            await _context.SaveChangesAsync();

            return todoItem;
        }
        #endregion

        private bool TodoItemExists(long id)
        {
            return _context.TodoItems.Any(e => e.Id == id);
        }
    }

    ////[Route("api/[controller]")]
    ////[ApiController]
    ////public class TodoController : ControllerBase
    ////{
    ////    private readonly TodoContext _context;

    ////    public TodoController(TodoContext context)
    ////    {
    ////        _context = context;

    ////        if (_context.TodoItems.Count() == 0)
    ////        {
    ////            // Create a new TodoItem if collection is empty,
    ////            // which means you can't delete all TodoItems.
    ////            _context.TodoItems.Add(new TodoItem { Name = "Item1" });
    ////            _context.SaveChanges();
    ////        }
    ////    }

    ////    // GET: api/Todo
    ////    [HttpGet]
    ////    public async Task<ActionResult<IEnumerable<TodoItem>>> GetTodoItems()
    ////    {
    ////        return await _context.TodoItems.ToListAsync();
    ////    }

    ////    // GET: api/Todo/5
    ////    [HttpGet("{id}")]
    ////    public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
    ////    {
    ////        var todoItem = await _context.TodoItems.FindAsync(id);

    ////        if (todoItem == null)
    ////        {
    ////            return NotFound();
    ////        }

    ////        return todoItem;
    ////    }

    ////    // POST: api/Todo
    ////    [HttpPost]
    ////    public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem item)
    ////    {
    ////        _context.TodoItems.Add(item);
    ////        await _context.SaveChangesAsync();

    ////        return CreatedAtAction(nameof(GetTodoItem), new { id = item.Id }, item);
    ////    }
    ////}
}