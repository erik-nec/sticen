﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NECITS.Models
{
    public class VehicleContext : DbContext
    {
        public VehicleContext(DbContextOptions<VehicleContext> options)
           : base(options)
        {
        }

        public DbSet<Vehicle> Vehicles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Vehicle>()
            //    .Property(b => b._SeatCount).HasColumnName("SeatCount");

            modelBuilder.Entity<Vehicle>(eb =>
            {
                eb.Property(b => b._SeatCount).HasColumnName("SeatCount");
                eb.Property(b => b._SeatCount).HasColumnType("jsonb");
                eb.Property(b => b._StopList).HasColumnName("StopList");
                eb.Property(b => b._StopList).HasColumnType("jsonb");
            });
        }
    }
}
