﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NECITS.Models
{
    public class Bus
    {
        public long VehicleId { get; set; }
        public long RouteId { get; set; }
        public string ServiceNo { get; set; }
        public bool IsComplete { get; set; }
    }
}
