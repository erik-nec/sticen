﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NECITS.Models
{
    public class BusContext: DbContext
    {
        public BusContext(DbContextOptions<BusContext> options)
           : base(options)
        {
        }

        public DbSet<Bus> Buses { get; set; }
    }
}
